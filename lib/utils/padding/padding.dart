/// Predefined padding sizes
///
abstract class CustomPadding {
  /// extra small = 4.0
  ///
  static const double xs = 4.0;

  /// small = 8.0
  ///
  static const double s = 8.0;

  /// medium = 12.0
  ///
  static const double m = 12.0;

  /// large = 16.0
  ///
  static const double l = 16.0;

  /// extra large = 24.0
  ///
  static const double xl = 24.0;

  /// extra, extra large = 32.0
  ///
  static const double xxl = 32.0;

  /// extra, extra, extra large
  ///
  static const double xxxl = 64.0;
}
