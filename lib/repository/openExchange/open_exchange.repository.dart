import 'package:dartz/dartz.dart';
import 'package:holiday_expentures/repository/openExchange/open_exchange.base.dart';
import 'package:holiday_expentures/repository/controller/convert/convert.controller.dart';
import 'package:holiday_expentures/utils/openExchangeKey/open_exchange.key.dart';
import 'package:http/http.dart';

class OpenExchangeRepository implements OpenExchangeBase {
  ///
  ///
  final Client _client = Client();

  ///
  ///
  final ConvertController _convertController = ConvertController();

  /// Fetch the latest exchange rates
  ///
  /// Returns a [Map] with a string as key and a double as value
  ///
  /// The key represents the currency type and the value the exchange rate
  ///
  ///
  ///
  /// Request from openexchangerates.org
  ///
  /// Documentation can be found at https://docs.openexchangerates.org/reference/latest-json
  ///
  /// The base exchange rate is USD
  ///
  /// The request returns a raw element with the following structure:
  ///
  /// {
  ///  "disclaimer": "Usage subject to terms: https://openexchangerates.org/terms",
  ///  "license": "https://openexchangerates.org/license",
  ///  "timestamp": 1682701200,
  ///  "base": "USD",
  ///  "rates": {
  ///      "AED": 3.6721,
  ///      "AFN": 86.499986,
  ///      "ALL": 100.95,
  ///      "AMD": 386.64,
  ///      "ANG": 1.802127,
  ///      "AOA": 509.5,
  ///      ....
  ///  }
  /// }
  ///
  @override
  Future<Either<String, Map<String, double>>> fetchLatestExchangeRates() async {
    try {
      const url =
          "https://openexchangerates.org/api/latest.json?app_id=$openExchangeKey";
      final response = await _client.get(Uri.parse(url));

      if (response.statusCode != 200) {
        return Future.value(left("Error"));
      }

      final body = _convertController.decodeBytes(response.bodyBytes);
      final Map<String, dynamic> rates = body['rates'];

      return Future.value(right(rates
          .map((key, value) => MapEntry(key, double.parse(value.toString())))));
    } catch (e) {
      return Future.value(left("Error"));
    }
  }

  /// Fetch all currency names with their unique abbreviation
  ///
  /// Returns a [Map] with the abbrevitation as key and the names as value
  ///
  ///
  /// Request from openexchangerates.org
  ///
  /// Documentation can be found at https://docs.openexchangerates.org/reference/currencies-json
  ///
  /// The request returns a json element, which looks like the following example:
  ///
  /// {
  ///  "AED": "United Arab Emirates Dirham",
  ///  "AFN": "Afghan Afghani",
  ///  "ALL": "Albanian Lek",
  ///  "AMD": "Armenian Dram",
  ///  "ANG": "Netherlands Antillean Guilder",
  ///  "AOA": "Angolan Kwanza",
  ///  "ARS": "Argentine Peso",
  ///  "AUD": "Australian Dollar",
  ///  ...
  /// }
  ///
  @override
  Future<Either<String, Map<String, String>>> fetchAllCurrencyNames() async {
    try {
      const url = "https://openexchangerates.org/api/currencies.json";
      final response = await _client.get(Uri.parse(url));

      if (response.statusCode != 200) {
        return Future.value(left("Error"));
      }

      final body = _convertController.decodeBytes(response.bodyBytes);
      final Map<String, dynamic> names = body;

      return Future.value(
          (right(names.map((key, value) => MapEntry(key, value.toString())))));
    } catch (e) {
      return Future.value(left("Error"));
    }
  }
}
