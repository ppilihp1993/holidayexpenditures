import 'package:dartz/dartz.dart';

abstract class OpenExchangeBase {
  Future<Either<String, Map<String, String>>> fetchAllCurrencyNames();
  Future<Either<String, Map<String, double>>> fetchLatestExchangeRates();
}
