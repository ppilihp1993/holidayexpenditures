abstract class HolidayFirestorePaths {
  /// Name of the holiday collection
  ///
  static const collection = 'holidays';

  /// Name of the sub collection expenditures
  ///
  static const subCollectionExpenditures = 'expenditures';

  /// Path to a document of the holiday collection
  ///
  static holidayDocument(String documentId) => '$collection/$documentId';

  /// Path to the expenditures sub collection
  ///
  static expendituresHolidayCollection(String holidayId) =>
      '${holidayDocument(holidayId)}/$subCollectionExpenditures';
}
