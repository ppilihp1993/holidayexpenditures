import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:holiday_expentures/repository/expenditure/model/expenditure.model.dart';
import 'package:holiday_expentures/repository/holiday/firestorePaths/holiday.firestore_paths.dart';
import 'package:holiday_expentures/repository/holiday/model/holiday.model.dart';

class HolidayRepository {
  /// Holiday collection reference
  ///
  final holidayCollectionRef = FirebaseFirestore.instance
      .collection(HolidayFirestorePaths.collection)
      .withConverter<Holiday>(
        fromFirestore: (snapshot, options) => Holiday.fromFirestore(snapshot),
        toFirestore: (value, options) => value.toFirestore(),
      );

  /// Returns all stored holiday elements
  ///
  Future<List<Holiday>> fetchAllHolidays() async {
    final querySnapshot = await holidayCollectionRef.get();
    final data = querySnapshot.docs.map((e) => e.data()).toList();
    return Future.value(data);
  }

  /// Returns a [Stream] of all [Holiday] element
  ///
  Stream<List<Holiday>> holidays$() {
    return holidayCollectionRef
        .snapshots()
        .map((event) => event.docs.map((e) => e.data()).toList());
  }

  /// Delete a holiday element
  ///
  Future deleteHoliday(String holidayId) async {
    final docRef = holidayCollectionRef.doc(holidayId);
    await docRef.delete();
  }

  /// Create a new holiday element
  ///
  Future<Either<String, Holiday>> createHoliday(Holiday holiday) async {
    final uniqueId = FirebaseFirestore.instance
        .collection(HolidayFirestorePaths.collection)
        .doc()
        .id;

    holiday.id = uniqueId;

    final docRef = holidayCollectionRef.doc(uniqueId);
    return docRef.set(holiday).then((value) => Future.value(right(holiday)),
        onError: (e) => Future.value(left("Error")));
  }

  /// Add a new expenditure to its holiday collection
  ///
  Future<Either<String, Expenditure>> addExpenditureToHoliday(
      String holidayId, Expenditure expenditure) async {
    final collectionRef = FirebaseFirestore.instance
        .collection(
            HolidayFirestorePaths.expendituresHolidayCollection(holidayId))
        .withConverter<Expenditure>(
          fromFirestore: (snapshot, options) =>
              Expenditure.fromFirestore(snapshot),
          toFirestore: (value, options) => value.toFirestore(),
        );

    final docRef = collectionRef.doc(expenditure.id);
    return docRef.set(expenditure).then(
        (value) => Future.value(right(expenditure)),
        onError: (e) => Future.value(left("Error")));
  }

  /// Delete an expenditure from ist holiday collection
  ///
  Future deleteExpenditureFromHoliday(
      String holidayId, String expenditureId) async {
    final collectionRef = FirebaseFirestore.instance
        .collection(
            HolidayFirestorePaths.expendituresHolidayCollection(holidayId))
        .withConverter<Expenditure>(
          fromFirestore: (snapshot, options) =>
              Expenditure.fromFirestore(snapshot),
          toFirestore: (value, options) => value.toFirestore(),
        );

    final docRef = collectionRef.doc(expenditureId);
    return docRef.delete();
  }

  /// Returns a [Stream] of all [Expenditure]s of a [Holiday] collection element
  ///
  Stream<List<Expenditure>> holidayExpenditures$(String holidayId) {
    final collectionRef = FirebaseFirestore.instance
        .collection(
            HolidayFirestorePaths.expendituresHolidayCollection(holidayId))
        .withConverter<Expenditure>(
          fromFirestore: (snapshot, options) =>
              Expenditure.fromFirestore(snapshot),
          toFirestore: (value, options) => value.toFirestore(),
        );

    return collectionRef
        .snapshots()
        .map((event) => event.docs.map((e) => e.data()).toList());
  }

  /// Returns all stored expenditures from one holiday elements
  ///
  Future<List<Expenditure>> fetchAllHolidayExpenditures(
      String holidayId) async {
    final collectionRef = FirebaseFirestore.instance
        .collection(
            HolidayFirestorePaths.expendituresHolidayCollection(holidayId))
        .withConverter<Expenditure>(
          fromFirestore: (snapshot, options) =>
              Expenditure.fromFirestore(snapshot),
          toFirestore: (value, options) => value.toFirestore(),
        );

    final querySnapshot = await collectionRef.get();
    final data = querySnapshot.docs.map((e) => e.data()).toList();
    return Future.value(data);
  }
}
