import 'package:cloud_firestore/cloud_firestore.dart';

class Holiday {
  /// Unique id
  ///
  String id;

  /// Name of the holiday
  ///
  final String name;

  /// Exchange rate for the holiday
  ///
  final double exchangeRate;

  /// Currency of the holiday country
  ///
  final String localCurrency;

  /// Home currency
  ///
  final String homeCurrency;

  /// Firestore model for a holiday element
  ///
  Holiday(this.id, this.name, this.exchangeRate, this.localCurrency,
      this.homeCurrency);

  /// Overloaded constructor for creating an element for firebase
  ///
  Holiday.firebase(
      this.name, this.exchangeRate, this.localCurrency, this.homeCurrency)
      : id = "";

  /// Convert a [DocumentSnapshot] directly to a [PropertyCosts] element
  ///
  factory Holiday.fromFirestore(
      DocumentSnapshot<Map<String, dynamic>> snapshot) {
    final data = snapshot.data() ?? {};
    return Holiday(data['id'], data['name'], data['exchange_rate'],
        data['local_currency'], data['home_currency']);
  }

  /// Convert a [Holiday] element to a firestore element
  ///
  Map<String, dynamic> toFirestore() => {
        'id': id,
        'name': name,
        'exchange_rate': exchangeRate,
        'local_currency': localCurrency,
        'home_currency': homeCurrency
      };
}
