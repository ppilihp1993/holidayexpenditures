import 'package:cloud_firestore/cloud_firestore.dart';

class ExpenditureTag {
  /// Unique id
  ///
  String id;

  /// Name of the expenditure tag
  ///
  final String tag;

  /// Firestore model for a expenditure tag element
  ///
  ExpenditureTag(this.id, this.tag);

  /// Overloaded constructor for a new element
  ///
  ExpenditureTag.firebase(this.tag) : id = "";

  /// Factory constructor for converting a firebase [DocumentSnapshot] directly to a [ExpenditureTag]
  ///
  factory ExpenditureTag.fromFirestore(
      DocumentSnapshot<Map<String, dynamic>> snapshot) {
    final data = snapshot.data() ?? {};
    return ExpenditureTag(data['id'], data['tag']);
  }

  /// Convert a [ExpenditureTag] into a firebase element
  ///
  Map<String, dynamic> toFirestore() => {
    'id': id,
    'tag': tag
  };
}
