abstract class ExpeditureTagsFirestorePaths {
  /// Name of the expenditure tags collection
  ///
  static const collection = 'expenditure_tags';
}