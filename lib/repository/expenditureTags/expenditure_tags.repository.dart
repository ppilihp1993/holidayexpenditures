import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:holiday_expentures/repository/expenditure/firestorePaths/expenditure.firestore_paths.dart';
import 'package:holiday_expentures/repository/expenditureTags/firestorePaths/expenditure_tags.firestore_paths.dart';
import 'package:holiday_expentures/repository/expenditureTags/model/expenditure_tag.model.dart';

class ExpenditureTagRepository {
  /// Firestore expenditure tags collection reference
  ///
  final expenditureTagCollectionRef = FirebaseFirestore.instance
      .collection(ExpeditureTagsFirestorePaths.collection)
      .withConverter<ExpenditureTag>(
        fromFirestore: (snapshot, options) =>
            ExpenditureTag.fromFirestore(snapshot),
        toFirestore: (value, options) => value.toFirestore(),
      );

  /// Create a new expenditure tag element
  ///
  Future<Either<String, ExpenditureTag>> createExpenditureTag(
      String tag) async {
    final uniqueId = FirebaseFirestore.instance
        .collection(ExpenditureFirestorePaths.collection)
        .doc()
        .id;

    final expenditureTag = ExpenditureTag(uniqueId, tag);

    final docRef = expenditureTagCollectionRef.doc(uniqueId);
    return docRef.set(expenditureTag).then(
        (value) => Future.value(right(expenditureTag)),
        onError: (e) => Future.value(left("Error")));
  }

  /// Delete an expenditure tag element
  ///
  Future deleteExpenditureTag(String id) async {
    final docRef = expenditureTagCollectionRef.doc(id);
    await docRef.delete();
  }

  /// Return all stored expenditure tags
  ///
  Future<List<ExpenditureTag>> fetchAllExpenditureTags() async {
    final querySnapshot = await expenditureTagCollectionRef.get();
    final data = querySnapshot.docs.map((e) => e.data()).toList();
    return Future.value(data);
  }
}
