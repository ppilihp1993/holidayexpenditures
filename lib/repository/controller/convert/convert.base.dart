import 'dart:typed_data';

abstract class ConvertBase {
  dynamic decodeBytes(Uint8List bodyBytes);
  Map<String, dynamic> decodeJson(dynamic data);
  String encodeJson(Map<String, dynamic> data);
}
