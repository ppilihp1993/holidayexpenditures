import 'dart:convert';
import 'dart:typed_data';

import 'package:holiday_expentures/repository/controller/convert/convert.base.dart';

class ConvertController implements ConvertBase {
  @override
  decodeBytes(Uint8List bodyBytes) {
    return json.decode(utf8.decode(bodyBytes));
  }

  @override
  Map<String, dynamic> decodeJson(data) {
    final decodedJson = json.decode(data);
    return decodedJson is Map<String, dynamic> ? decodedJson : {};
  }

  @override
  String encodeJson(Map<String, dynamic> data) {
    return json.encode(data);
  }
}
