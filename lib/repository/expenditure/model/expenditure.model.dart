import 'package:cloud_firestore/cloud_firestore.dart';

class Expenditure {
  /// Unique id
  ///
  String id;

  /// Name of the expenditure
  ///
  final String name;

  /// Expenditure in the local currency
  ///
  final double expenditureLocalCurrency;

  /// Expenditure in the home currency
  ///
  final double expeditureHomeCurrency;

  /// Id of the related holiday element
  ///
  final String holidayId;

  /// Timestamp of the creation in milliseconds
  ///
  final int timestamp;

  /// A tag which describes the type of the expenditure
  ///
  final String tag;

  /// Firestore model for a expenditure element
  ///
  Expenditure(this.id, this.name, this.expenditureLocalCurrency,
      this.expeditureHomeCurrency, this.holidayId, this.timestamp,
      [this.tag = ""]);

  /// Overloaded constructfor a new [Expenditure] firebase element
  ///
  Expenditure.firebase(this.name, this.expenditureLocalCurrency,
      this.expeditureHomeCurrency, this.holidayId,
      [this.tag = ""])
      : id = "",
        timestamp = DateTime.now().millisecondsSinceEpoch;

  /// Empty overloaded constructor
  ///
  Expenditure.empty()
      : id = "",
        name = "",
        expenditureLocalCurrency = 0,
        expeditureHomeCurrency = 0,
        holidayId = "",
        timestamp = 0,
        tag = "";

  /// Convert a [DocumentSnapshot] directly to a [PropertyCosts] element
  ///
  factory Expenditure.fromFirestore(
      DocumentSnapshot<Map<String, dynamic>> snapshot) {
    final data = snapshot.data() ?? {};
    return Expenditure(
        data['id'],
        data['name'],
        data['expenditure_local_currency'],
        data['expenditure_home_currency'],
        data['holiday_id'],
        data['timestamp'],
        data['tag'] ?? "");
  }

  /// Convert a [Expenditure] element to a firestore element
  ///
  Map<String, dynamic> toFirestore() => {
        'id': id,
        'name': name,
        'expenditure_local_currency': expenditureLocalCurrency,
        'expenditure_home_currency': expeditureHomeCurrency,
        'holiday_id': holidayId,
        'timestamp': timestamp,
        'tag': tag
      };
}
