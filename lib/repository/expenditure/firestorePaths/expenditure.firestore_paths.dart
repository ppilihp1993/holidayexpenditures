abstract class ExpenditureFirestorePaths {
  /// Name of the expenture collection
  ///
  static const collection = 'expenditures';

  /// Path to a document of the holiday collection
  ///
  static expeditureDocument(String documentId) => '$collection/$documentId';
}
