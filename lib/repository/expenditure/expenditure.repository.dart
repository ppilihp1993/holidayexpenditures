import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:holiday_expentures/repository/expenditure/firestorePaths/expenditure.firestore_paths.dart';
import 'package:holiday_expentures/repository/expenditure/model/expenditure.model.dart';

class ExpenditureRepository {
  /// Expenditure collection reference
  ///
  final expenditureCollectionRef = FirebaseFirestore.instance
      .collection(ExpenditureFirestorePaths.collection)
      .withConverter<Expenditure>(
        fromFirestore: (snapshot, options) =>
            Expenditure.fromFirestore(snapshot),
        toFirestore: (value, options) => value.toFirestore(),
      );

  /// Create a new expenditure element
  ///
  Future<Either<String, Expenditure>> createExpenditure(
      Expenditure expenditure) async {
    final uniqueId = FirebaseFirestore.instance
        .collection(ExpenditureFirestorePaths.collection)
        .doc()
        .id;

    expenditure.id = uniqueId;

    final docRef = expenditureCollectionRef.doc(uniqueId);
    return docRef.set(expenditure).then(
        (value) => Future.value(right(expenditure)),
        onError: (e) => Future.value(left("Error")));
  }

  /// Delete an expenditure element
  ///
  Future deleteExpenditure(String id) async {
    final docRef = expenditureCollectionRef.doc(id);
    await docRef.delete();
  }

  /// Update a complete expenditure element
  ///
  Future<Either<String, Expenditure>> updateExpenditure(
      Expenditure expenditure) async {
    final docRef = expenditureCollectionRef.doc(expenditure.id);
    return docRef.set(expenditure).then(
        (value) => Future.value(right(expenditure)),
        onError: (e) => Future.value(left("Error")));
  }
}
