import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:holiday_expentures/firebase_options.dart';
import 'package:holiday_expentures/pages/expenditures/expenditures.page.dart';
import 'package:holiday_expentures/pages/holidays/holidays.page.dart';
import 'package:holiday_expentures/service/authentication/authentication.service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  await AuthenticationService().authenticate();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Holiday Expentures',
      theme: appTheme(context),
      home: const HolidaysPage(),
      routes: <String, WidgetBuilder>{
        HolidaysPage.route: (context) => const HolidaysPage(),
        ExpenturesPage.route: (context) => const ExpenturesPage()
      },
    );
  }
}

///
///
ThemeData appTheme(BuildContext context) => ThemeData(
    useMaterial3: true,
    colorScheme: ColorScheme.light(
        background: Colors.white, primary: Colors.cyan[700] ?? Colors.cyan),
    elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
      backgroundColor: MaterialStateProperty.resolveWith((states) {
        if (states.contains(MaterialState.disabled)) {
          return Colors.grey;
        } else {
          return Colors.cyan[300];
        }
      }),
      shape: MaterialStateProperty.all(
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(25))),
    )));
