import 'package:uuid/uuid.dart';

class ExpentureVM {
  final String title;
  final num expentureEuro;
  final num expentureEgp;
  final String id;

  ExpentureVM(
      {required this.title,
      required this.expentureEuro,
      required this.expentureEgp})
      : id = const Uuid().v1();

  ExpentureVM.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        expentureEuro = json['expenture_euro'],
        expentureEgp = json['expenture_egp'],
        id = json['id'];

  Map<String, dynamic> toJson() => {
        'title': title,
        'expenture_euro': expentureEuro,
        'expenture_egp': expentureEgp,
        'id': id
      };
}
