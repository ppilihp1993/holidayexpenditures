import 'package:firebase_auth/firebase_auth.dart';

class AuthenticationService {
  /// Anonymous user credentials
  ///
  UserCredential? userCredential;

  static final AuthenticationService _instance =
      AuthenticationService._internal();
  factory AuthenticationService() => _instance;
  AuthenticationService._internal();

  /// Login as an anonym user
  ///
  Future<void> authenticate() async {
    final kUserCredential = await FirebaseAuth.instance.signInAnonymously();
    userCredential = kUserCredential;
    return Future.value();
  }
}
