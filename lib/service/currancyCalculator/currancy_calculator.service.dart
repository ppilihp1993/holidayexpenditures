import 'package:holiday_expentures/service/currancyCalculator/currancy_calculator.base.dart';

class CurrancyCalculatorService implements CurrancyCalculatorBase {
  @override
  num convert(num expenture) {
    final conversion = expenture * 0.052;
    String inString = conversion.toStringAsFixed(2); // '2.35'
    return double.parse(inString);
  }
}
