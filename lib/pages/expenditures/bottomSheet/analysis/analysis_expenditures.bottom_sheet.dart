import 'package:flutter/material.dart';
import 'package:holiday_expentures/pages/expenditures/bottomSheet/analysis/controller/analysis.controller.dart';
import 'package:holiday_expentures/pages/expenditures/bottomSheet/analysis/viewModel/analysis.vm.dart';
import 'package:holiday_expentures/pages/expenditures/bottomSheet/analysis/widgets/analysisChart/analysis_chart.dart';
import 'package:holiday_expentures/pages/expenditures/bottomSheet/analysis/widgets/analysisListing/analysis_listing.dart';
import 'package:holiday_expentures/pages/expenditures/service/expenditures.service.dart';
import 'package:holiday_expentures/utils/padding/padding.dart';
import 'package:holiday_expentures/widgets/buttons/customElevatedButton/custom_elevated_button.dart';
import 'package:rive/rive.dart';

class AnalyisiExpendituresBottomSheet extends StatelessWidget {
  /// Displays analysis information if the [Expenditure]s
  ///
  const AnalyisiExpendituresBottomSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FractionallySizedBox(
      heightFactor: 0.92,
      widthFactor: 1.0,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
                padding: const EdgeInsets.all(CustomPadding.m),
                child: Column(
                  children: [
                    const Text("Analysis"),
                    const SizedBox(
                      height: CustomPadding.xxxl,
                    ),
                    FutureBuilder<AnalysisVm>(
                      future: AnalysisController().createAnalysis(
                          ExpendituresService().holiday?.id ?? ""),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.done) {
                          final data = snapshot.data;
                          return data != null
                              ? Column(
                                  children: [
                                    AnalysisChart(analysisData: data),
                                    const SizedBox(
                                      height: CustomPadding.xxxl,
                                    ),
                                    AnalysisListing(analysisData: data)
                                  ],
                                )
                              : Container();
                        } else {
                          return const Center(
                            child: RiveAnimation.asset(
                                'assets/riveAnimations/loadingbars.riv'),
                          );
                        }
                      },
                    ),
                    const SizedBox(
                      height: CustomPadding.xxl,
                    ),
                    CustomElevatedButton(
                      onTap: () => Navigator.of(context).pop(),
                      text: "Close",
                    )
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
