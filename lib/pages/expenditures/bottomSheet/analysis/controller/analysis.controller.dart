import 'dart:math';

import 'package:flutter/material.dart';
import 'package:holiday_expentures/pages/expenditures/bottomSheet/analysis/viewModel/analysis.vm.dart';
import 'package:holiday_expentures/pages/expenditures/bottomSheet/analysis/viewModel/analysis_listing.vm.dart';
import 'package:holiday_expentures/repository/expenditure/model/expenditure.model.dart';
import 'package:holiday_expentures/repository/holiday/holiday.repository.dart';
import 'package:collection/collection.dart';
import 'package:rxdart/subjects.dart';

class AnalysisController {
  ///
  ///
  BehaviorSubject<List<AnalysisListingVm>> analysisListings$ =
      BehaviorSubject();

  /// Create all analysis information
  ///
  Future<AnalysisVm> createAnalysis(String holidayId) async {
    final expenditures =
        await HolidayRepository().fetchAllHolidayExpenditures(holidayId);

    final tags = expenditures.map((e) => e.tag).toSet().toList();

    final expendituresTagMap = Map.fromEntries(tags.map((e) => MapEntry(
        e,
        expenditures
            .where((element) => element.tag.toLowerCase() == e.toLowerCase())
            .toList())));

    final random = Random();
    final tagColorsMap = Map.fromEntries(tags.map((e) => MapEntry(
        e, Colors.primaries[random.nextInt(Colors.primaries.length)])));

    return AnalysisVm(
        tagExpendituresMap: expendituresTagMap, tagColorsMap: tagColorsMap);
  }

  /// Sum all [Expenditure]s home currency values
  ///
  /// Returns a [Map] with the tag [String] as key and the summary of the [expenditureHomeCurrency] as [double]
  ///
  Map<String, double> sumExpenditures(
      Map<String, List<Expenditure>> expendituresTagMap) {
    return Map.fromEntries(expendituresTagMap.entries.map((e) =>
        MapEntry(e.key, e.value.map((e) => e.expeditureHomeCurrency).sum)));
  }

  /// Generate a [List] from type [AnalysisListingVm] and push the data to [analysisListings$]
  ///
  void generateAnalysisListingData(Map<String, List<Expenditure>> data) {
    final analysisListingData = data.entries
        .map((e) => AnalysisListingVm(
            isExpanded: false, tag: e.key, expenditures: e.value))
        .toList();

    analysisListings$.add(analysisListingData);
  }

  /// Update the [isExpanded] parameter from one element
  ///
  void updateAnalysisListingData(int index, bool isExpanded) {
    if (analysisListings$.hasValue) {
      final data = [...analysisListings$.value];
      data[index].isExpanded = !isExpanded;

      analysisListings$.add(data);
    }
  }
}
