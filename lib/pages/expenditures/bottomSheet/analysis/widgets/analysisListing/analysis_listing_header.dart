import 'package:flutter/material.dart';
import 'package:holiday_expentures/pages/expenditures/bottomSheet/analysis/viewModel/analysis_listing.vm.dart';
import 'package:holiday_expentures/pages/expenditures/service/expenditures.service.dart';
import 'package:holiday_expentures/utils/padding/padding.dart';
import 'package:collection/collection.dart';

class AnalysisListingHeader extends StatelessWidget {
  /// Selected [AnalysisListingVm] element
  ///
  final AnalysisListingVm analysisListingVm;

  /// All tag colors
  ///
  final Map<String, Color> tagColors;

  /// Header element of the [AnalysisListing] [ExpansionPanel]
  ///
  const AnalysisListingHeader(
      {Key? key, required this.analysisListingVm, required this.tagColors})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(CustomPadding.s),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 32,
            width: 32,
            decoration: BoxDecoration(
                color: tagColors[analysisListingVm.tag],
                borderRadius: BorderRadius.circular(32)),
          ),
          const SizedBox(
            width: CustomPadding.m,
          ),
          Text(
            analysisListingVm.tag.isEmpty ? "Other" : analysisListingVm.tag,
            style: Theme.of(context).textTheme.titleMedium,
          ),
          const SizedBox(
            width: CustomPadding.m,
          ),
          Expanded(
            child: Text(analysisListingVm.expenditures
                    .map((e) => e.expeditureHomeCurrency)
                    .toList()
                    .sum
                    .toStringAsFixed(2) +
                " " +
                (ExpendituresService().holiday?.homeCurrency ?? "")),
          )
        ],
      ),
    );
  }
}
