import 'package:flutter/material.dart';
import 'package:holiday_expentures/pages/expenditures/bottomSheet/analysis/viewModel/analysis_listing.vm.dart';
import 'package:holiday_expentures/pages/expenditures/service/expenditures.service.dart';
import 'package:holiday_expentures/utils/padding/padding.dart';

class AnalysisListingBody extends StatelessWidget {
  /// Selected [AnalysisListingVm] element
  ///
  final AnalysisListingVm analysisListingVm;

  /// Lists all expenditures of one tag
  ///
  const AnalysisListingBody({Key? key, required this.analysisListingVm})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: analysisListingVm.expenditures
          .map((e) => Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: analysisListingVm.expenditures.last == e
                            ? BorderSide.none
                            : const BorderSide(
                                width: 1, color: Colors.black26))),
                child: Padding(
                  padding: const EdgeInsets.all(CustomPadding.s),
                  child: Row(
                    children: [
                      Text(e.name),
                      const SizedBox(
                        width: CustomPadding.xxl,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(e.expeditureHomeCurrency.toStringAsFixed(2) +
                                " " +
                                (ExpendituresService().holiday?.homeCurrency ??
                                    "")),
                            Text(e.expenditureLocalCurrency.toStringAsFixed(2) +
                                " " +
                                (ExpendituresService().holiday?.localCurrency ??
                                    ""))
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ))
          .toList(),
    );
  }
}
