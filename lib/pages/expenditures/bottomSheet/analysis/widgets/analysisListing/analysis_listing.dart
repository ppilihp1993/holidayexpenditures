import 'package:flutter/material.dart';
import 'package:holiday_expentures/pages/expenditures/bottomSheet/analysis/controller/analysis.controller.dart';
import 'package:holiday_expentures/pages/expenditures/bottomSheet/analysis/viewModel/analysis.vm.dart';
import 'package:holiday_expentures/pages/expenditures/bottomSheet/analysis/viewModel/analysis_listing.vm.dart';
import 'package:holiday_expentures/pages/expenditures/bottomSheet/analysis/widgets/analysisListing/analysis_listing_body.dart';
import 'package:holiday_expentures/pages/expenditures/bottomSheet/analysis/widgets/analysisListing/analysis_listing_header.dart';

class AnalysisListing extends StatefulWidget {
  /// Generated analysis data
  ///
  final AnalysisVm analysisData;

  ///
  ///
  const AnalysisListing({Key? key, required this.analysisData})
      : super(key: key);

  @override
  State<AnalysisListing> createState() => _AnalysisListingState();
}

class _AnalysisListingState extends State<AnalysisListing> {
  /// Instance of [AnalysisController]
  ///
  final _controller = AnalysisController();

  @override
  void initState() {
    _controller
        .generateAnalysisListingData(widget.analysisData.tagExpendituresMap);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<AnalysisListingVm>>(
      initialData: const [],
      stream: _controller.analysisListings$,
      builder: (context, snapshot) {
        final data = snapshot.data ?? [];

        return ExpansionPanelList(
          elevation: 0,
          expansionCallback: (panelIndex, isExpanded) =>
              _controller.updateAnalysisListingData(panelIndex, isExpanded),
          children: data
              .map((e) => ExpansionPanel(
                  isExpanded: e.isExpanded,
                  headerBuilder: (context, isExpanded) => AnalysisListingHeader(
                      analysisListingVm: e,
                      tagColors: widget.analysisData.tagColorsMap),
                  body: AnalysisListingBody(analysisListingVm: e)))
              .toList(),
        );
      },
    );
  }
}
