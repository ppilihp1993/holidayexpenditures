import 'package:flutter/material.dart';
import 'package:holiday_expentures/pages/expenditures/bottomSheet/analysis/controller/analysis.controller.dart';
import 'package:holiday_expentures/pages/expenditures/bottomSheet/analysis/viewModel/analysis.vm.dart';
import 'package:pie_chart/pie_chart.dart';

class AnalysisChart extends StatelessWidget {
  /// Generated analysis data
  ///
  final AnalysisVm analysisData;

  /// Displays a [PieChart] with the information from the [AnalysisVm]
  ///
  const AnalysisChart({Key? key, required this.analysisData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PieChart(
      dataMap: AnalysisController().sumExpenditures(analysisData.tagExpendituresMap),
      animationDuration: const Duration(milliseconds: 800),
      chartLegendSpacing: 32,
      chartRadius: MediaQuery.of(context).size.width / 2.5,
      colorList: analysisData.tagColorsMap.values.toList(),
      initialAngleInDegree: 0,
      chartType: ChartType.ring,
      ringStrokeWidth: 32,
      centerText: "Expenditures",
      legendOptions: const LegendOptions(
        showLegendsInRow: false,
        legendPosition: LegendPosition.bottom,
        showLegends: false,
        legendShape: BoxShape.circle,
        legendTextStyle: TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
      chartValuesOptions: const ChartValuesOptions(
        showChartValueBackground: true,
        showChartValues: true,
        showChartValuesInPercentage: true,
        showChartValuesOutside: true,
        decimalPlaces: 1,
      ),
    );
  }
}
