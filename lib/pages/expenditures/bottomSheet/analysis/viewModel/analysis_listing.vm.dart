import 'package:holiday_expentures/repository/expenditure/model/expenditure.model.dart';

class AnalysisListingVm {
  /// Is the listing expanded or not
  ///
  bool isExpanded;

  /// Tag of the expenditures
  ///
  final String tag;

  /// List with all [Expenditure]s
  ///
  final List<Expenditure> expenditures;

  ///
  ///
  AnalysisListingVm(
      {required this.isExpanded,
      required this.tag,
      required this.expenditures});
}
