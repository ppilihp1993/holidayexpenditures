import 'dart:ui';

import 'package:holiday_expentures/repository/expenditure/model/expenditure.model.dart';

class AnalysisVm {
  /// A [Map] with all expenditures of each tag
  ///
  /// Key is the tag as [String]
  ///
  /// Value is a [List] of [Expenditures]
  ///
  final Map<String, List<Expenditure>> tagExpendituresMap;

  /// A [Map] with random colors for each tag
  ///
  /// Key ist the tag as [String]
  ///
  /// Value is the [Color]
  ///
  final Map<String, Color> tagColorsMap;

  /// Corresponding view model for the [AnalysisExpendituresBottomSheet]
  ///
  /// Stores two [Map]s with the expenditures and colors for each tag
  ///
  const AnalysisVm(
      {required this.tagExpendituresMap, required this.tagColorsMap});
}
