import 'package:flutter/material.dart';
import 'package:holiday_expentures/pages/expenditures/bottomSheet/addExpenditureBottomSheet/expenditureTag/expenditure_tag.dart';
import 'package:holiday_expentures/pages/expenditures/service/expenditures.service.dart';
import 'package:holiday_expentures/repository/expenditure/model/expenditure.model.dart';
import 'package:holiday_expentures/utils/padding/padding.dart';
import 'package:holiday_expentures/widgets/bottomSheetTemplate/custom_bottom_sheet_template.dart';
import 'package:holiday_expentures/widgets/buttons/customElevatedButton/custom_elevated_button.dart';

class AddExpenditureBottomSheet extends StatefulWidget {
  /// Add a [Expenditure] element for editing
  ///
  final Expenditure? expenditure;

  /// Instance of [TextEditingController] for the expenditure
  ///
  final TextEditingController _textEditingControllerExpenditure;

  /// Instance of [TextEditingController] for the name
  ///
  final TextEditingController _textEditingControllerName;

  ///
  ///
  AddExpenditureBottomSheet({Key? key, this.expenditure})
      : _textEditingControllerExpenditure = TextEditingController(
            text: expenditure?.expenditureLocalCurrency.toString() ?? ""),
        _textEditingControllerName =
            TextEditingController(text: expenditure?.name ?? ""),
        super(key: key);

  @override
  State<AddExpenditureBottomSheet> createState() =>
      _AddExpenditureBottomSheetState();
}

class _AddExpenditureBottomSheetState extends State<AddExpenditureBottomSheet> {
  String expenditureTag = "";

  @override
  Widget build(BuildContext context) {
    return CustomBottomSheetTemplate(
      child: Column(
        children: [
          Text(
            "Create expenditure",
            style: Theme.of(context).textTheme.titleMedium,
          ),
          const SizedBox(
            height: CustomPadding.l,
          ),
          TextField(
            controller: widget._textEditingControllerName,
            textCapitalization: TextCapitalization.words,
            decoration:
                const InputDecoration(label: Text("Name of the expenditure")),
          ),
          const SizedBox(
            height: CustomPadding.l,
          ),
          TextField(
            controller: widget._textEditingControllerExpenditure,
            keyboardType: TextInputType.number,
            decoration: const InputDecoration(
                label: Text("Expenditure in the local currency")),
          ),
          const SizedBox(
            height: CustomPadding.l,
          ),
          ExpenditureTagWidget(
            initialTag: widget.expenditure?.tag ?? "",
            tagCallback: (tag) => expenditureTag = tag,
          ),
          const SizedBox(
            height: CustomPadding.l,
          ),
          CustomElevatedButton(
            text: "Save",
            onTap: () => ExpendituresService().save(
                widget._textEditingControllerName.text,
                widget._textEditingControllerExpenditure.text,
                expenditureTag,
                context,
                widget.expenditure),
          )
        ],
      ),
    );
  }
}
