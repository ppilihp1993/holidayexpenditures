import 'package:flutter/material.dart';
import 'package:holiday_expentures/pages/expenditures/bottomSheet/addExpenditureBottomSheet/expenditureTag/expenditure_tag_grid.dart';
import 'package:holiday_expentures/utils/padding/padding.dart';

class ExpenditureTagWidget extends StatefulWidget {
  /// Returns the active tag
  ///
  final Function(String tag) tagCallback;

  /// Initial expenditure tag
  ///
  final String initialTag;

  /// Displays the UI for adding a new [ExpenditureTag]
  ///
  const ExpenditureTagWidget(
      {Key? key, required this.tagCallback, this.initialTag = ""})
      : super(key: key);

  @override
  State<ExpenditureTagWidget> createState() => _ExpenditureTagWidgetState();
}

class _ExpenditureTagWidgetState extends State<ExpenditureTagWidget> {
  /// Instance of [TextEditingController] for the expenditure
  ///
  final TextEditingController textEditingController = TextEditingController();

  @override
  void initState() {
    textEditingController
        .addListener(() => widget.tagCallback.call(textEditingController.text));
    if (widget.initialTag.isNotEmpty) {
      textEditingController.text = widget.initialTag;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextField(
            controller: textEditingController,
            textCapitalization: TextCapitalization.words,
            decoration: const InputDecoration(label: Text("Expenditure tag"))),
        const SizedBox(
          height: CustomPadding.m,
        ),
        ExpenditureTagGrid(
            expenditureTagEditingController: textEditingController)
      ],
    );
  }
}
