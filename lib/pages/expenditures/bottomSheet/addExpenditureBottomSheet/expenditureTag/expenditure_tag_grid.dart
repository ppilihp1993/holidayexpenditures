import 'package:flutter/material.dart';
import 'package:holiday_expentures/repository/expenditureTags/expenditure_tags.repository.dart';
import 'package:holiday_expentures/repository/expenditureTags/model/expenditure_tag.model.dart';
import 'package:holiday_expentures/utils/padding/padding.dart';

class ExpenditureTagGrid extends StatelessWidget {
  /// Instance of the [TextEditingController] from [ExpenditureTagWidget]
  ///
  final TextEditingController expenditureTagEditingController;

  /// [Wrap] widget with all [ExpenditureTag] elements from the [ExpenditureTagRepository]
  ///
  const ExpenditureTagGrid(
      {Key? key, required this.expenditureTagEditingController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<ExpenditureTag>>(
      initialData: const [],
      future: ExpenditureTagRepository().fetchAllExpenditureTags(),
      builder: (context, snapshot) {
        final data = snapshot.data ?? [];

        return data.isEmpty
            ? const SizedBox()
            : Column(
                children: [
                  Text(
                    "Available tags",
                    style: Theme.of(context).textTheme.labelSmall,
                  ),
                  Wrap(
                    spacing: CustomPadding.s,
                    children: data
                        .map((e) => GestureDetector(
                            onTap: () {
                              expenditureTagEditingController.text = e.tag;
                            },
                            child: Chip(label: Text(e.tag))))
                        .toList(),
                  ),
                ],
              );
      },
    );
  }
}
