import 'package:flutter/material.dart';
import 'package:holiday_expentures/controller/bottomSheet/bottom_sheet.controller.dart';
import 'package:holiday_expentures/pages/expenditures/bottomSheet/analysis/analysis_expenditures.bottom_sheet.dart';
import 'package:holiday_expentures/pages/expenditures/service/expenditures.service.dart';
import 'package:holiday_expentures/pages/expenditures/widgets/addExpenditure/add_expenditure.dart';
import 'package:holiday_expentures/pages/expenditures/widgets/content/expenditures_content.dart';
import 'package:holiday_expentures/repository/holiday/model/holiday.model.dart';

class ExpenturesPage extends StatelessWidget {
  /// Route name
  ///
  static String route = "/expenditures";

  ///
  ///
  const ExpenturesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Holiday holiday =
        ModalRoute.of(context)!.settings.arguments as Holiday;
    ExpendituresService().holiday = holiday;

    return Scaffold(
      appBar: AppBar(
        title: Text(ExpendituresService().holiday?.name ?? ""),
        actions: [
          IconButton(
              onPressed: () => BottomSheetController().showBottomSheet(
                  const AnalyisiExpendituresBottomSheet(), context),
              icon: const Icon(
                Icons.pie_chart,
              ))
        ],
      ),
      body: const ExpendituresContent(),
      floatingActionButton: const AddExpenditure(),
    );
  }
}
