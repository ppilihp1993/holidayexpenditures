import 'package:flutter/material.dart';
import 'package:holiday_expentures/controller/bottomSheet/bottom_sheet.controller.dart';
import 'package:holiday_expentures/repository/expenditure/expenditure.repository.dart';
import 'package:holiday_expentures/repository/expenditure/model/expenditure.model.dart';
import 'package:holiday_expentures/repository/expenditureTags/expenditure_tags.repository.dart';
import 'package:holiday_expentures/repository/holiday/holiday.repository.dart';
import 'package:holiday_expentures/repository/holiday/model/holiday.model.dart';
import 'package:holiday_expentures/widgets/deleteBottomSheet/delete_bottom_sheet.dart';

class ExpendituresService {
  Holiday? holiday;

  static final ExpendituresService _instance = ExpendituresService._internal();
  factory ExpendituresService() => _instance;
  ExpendituresService._internal();

  /// Create a new [Expenditure] element
  ///
  /// Add the expenditure to the expenditures collection - Source of truth
  ///
  /// Add the expenditure to the expenditure sub collection of the holiday element
  /// This task is usually performed by a Cloud function
  ///
  // void create(String name, String expenditureLocalCurrency,
  //     BuildContext context) async {
  //   if (expenditureLocalCurrency.isEmpty || holiday == null) {
  //     /// error
  //   }

  //   double localExpenditure = double.parse(expenditureLocalCurrency);
  //   double homeExpenditure = double.parse(
  //       (localExpenditure / (holiday?.exchangeRate ?? 1)).toStringAsFixed(3));

  //   final expenditure = Expenditure.firebase(
  //       name, localExpenditure, homeExpenditure, holiday?.id ?? "");

  //   final result = await ExpenditureRepository().createExpenditure(expenditure);
  //   if (result.isRight()) {
  //     final expenditureResult = result.getOrElse(() => Expenditure.empty());
  //     await HolidayRepository()
  //         .addExpenditureToHoliday(holiday?.id ?? "", expenditureResult);
  //   } else {
  //     /// error
  //   }

  //   Navigator.of(context).pop();
  // }

  /// Delete an [Expenditure]
  ///
  /// Delete the expenditure from the expenditures collection (Source of truth)
  ///
  /// Delete the expenditure from the expenditure sub collection of a holiday element
  /// This task is usually performed by a Cloud function
  ///
  void delete(String expenditureId, BuildContext context) async {
    final result = await BottomSheetController()
        .showBottomSheet(const DeleteBottomSheet(), context);

    if (result) {
      await ExpenditureRepository().deleteExpenditure(expenditureId);
      await HolidayRepository()
          .deleteExpenditureFromHoliday(holiday?.id ?? "", expenditureId);
    }
  }

  /// Save an expenditure element
  ///
  /// If the optional parameter [expenditureId] is set, an available element will be updated
  ///
  /// Add the expenditure to the expenditures collection - Source of truth
  ///
  /// Add the expenditure to the expenditure sub collection of the holiday element
  /// This task is usually performed by a Cloud function
  ///
  void save(String name, String expenditureLocalCurrency, String tag,
      BuildContext context,
      [Expenditure? expenditure]) async {
    if (expenditureLocalCurrency.isEmpty || holiday == null) {
      /// error
    }

    double localExpenditure = double.parse(expenditureLocalCurrency);
    double homeExpenditure = double.parse(
        (localExpenditure / (holiday?.exchangeRate ?? 1)).toStringAsFixed(3));

    final newExpenditure = expenditure != null
        ? _editExpenditure(
            name, localExpenditure, homeExpenditure, expenditure, tag)
        : _createNew(name, localExpenditure, homeExpenditure, tag);

    _storeTag(tag);

    final result = expenditure != null
        ? await ExpenditureRepository().updateExpenditure(newExpenditure)
        : await ExpenditureRepository().createExpenditure(newExpenditure);
    if (result.isRight()) {
      final expenditureResult = result.getOrElse(() => Expenditure.empty());
      await HolidayRepository()
          .addExpenditureToHoliday(holiday?.id ?? "", expenditureResult);
    } else {
      /// error
    }

    Navigator.of(context).pop();
  }

  /// Returns a new [Expenditure] element
  ///
  Expenditure _createNew(String name, double localExpenditure,
          double homeExpenditure, String tag) =>
      Expenditure.firebase(
          name, localExpenditure, homeExpenditure, holiday?.id ?? "", tag);

  /// Returns a new [Expenditure] with the data from an existing
  ///
  Expenditure _editExpenditure(String name, double localExpenditure,
          double homeExpenditure, Expenditure expenditure, String tag) =>
      Expenditure(expenditure.id, name, localExpenditure, homeExpenditure,
          holiday?.id ?? "", expenditure.timestamp, tag);
  
  /// Check if a tag is available and store it if not
  ///
  void _storeTag(String tag) async {
    final tags = await ExpenditureTagRepository().fetchAllExpenditureTags();
    if (!tags.any((element) => element.tag.toLowerCase() == tag.toLowerCase())) {
      ExpenditureTagRepository().createExpenditureTag(tag);
    }
  }
}
