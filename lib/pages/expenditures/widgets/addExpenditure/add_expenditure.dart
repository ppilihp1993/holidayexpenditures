import 'package:flutter/material.dart';
import 'package:holiday_expentures/controller/bottomSheet/bottom_sheet.controller.dart';
import 'package:holiday_expentures/pages/expenditures/bottomSheet/addExpenditureBottomSheet/add_expenditure.bottom_sheet.dart';
import 'package:holiday_expentures/widgets/buttons/customFloatingActionButton/custom_floating_action_button.dart';

class AddExpenditure extends StatelessWidget {
  /// Open the [AddExpenditureBottomSheet] bottom sheet for creating a new expenditure
  ///
  const AddExpenditure({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomFloatingActionButton(
      callback: () => BottomSheetController()
          .showBottomSheet(AddExpenditureBottomSheet(), context),
    );
  }
}
