import 'package:flutter/material.dart';
import 'package:holiday_expentures/controller/snackbar/custom_snackbar.dart';
import 'package:intl/intl.dart';

class ExpendituresListItemDate extends StatelessWidget {
  /// Creation date in milliseconds
  ///
  final int creationDate;

  /// Displays the creation date
  ///
  const ExpendituresListItemDate({Key? key, required this.creationDate})
      : super(key: key);

  /// Get the day of the month
  ///
  String _getDay() => _parseDate('dd');

  /// Get only the first three letters of the month
  ///
  String _getMonth() => _parseDate('MMM');

  /// Get the full date
  ///
  String _getFullDate() => _parseDate('HH:mm - dd.MM.yyyy');

  /// Parse the creation date to a formatted string
  ///
  String _parseDate(String dateFormatType) {
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(creationDate);
    return DateFormat(dateFormatType).format(dateTime);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => CustomSnackBar.of(context).showDefault(_getFullDate()),
      child: Column(
        children: [
          Text(
            _getDay(),
            style: Theme.of(context).textTheme.titleLarge,
          ),
          Text(
            _getMonth(),
            style: Theme.of(context).textTheme.titleLarge,
          )
        ],
      ),
    );
  }
}
