import 'package:flutter/material.dart';
import 'package:holiday_expentures/controller/bottomSheet/bottom_sheet.controller.dart';
import 'package:holiday_expentures/pages/expenditures/bottomSheet/addExpenditureBottomSheet/add_expenditure.bottom_sheet.dart';
import 'package:holiday_expentures/pages/expenditures/service/expenditures.service.dart';
import 'package:holiday_expentures/pages/expenditures/widgets/content/list/item/expenditures_list_item_date.dart';
import 'package:holiday_expentures/repository/expenditure/model/expenditure.model.dart';
import 'package:holiday_expentures/utils/padding/padding.dart';

class ExpendituresListItem extends StatelessWidget {
  /// Expenditure ist element
  ///
  final Expenditure expenditure;

  /// Displays all information of an [Expenditure] element
  ///
  const ExpendituresListItem({Key? key, required this.expenditure})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(CustomPadding.xs),
          color: const Color.fromARGB(255, 229, 221, 248)),
      child: Padding(
        padding: const EdgeInsets.all(CustomPadding.s),
        child: Row(
          children: [
            ExpendituresListItemDate(creationDate: expenditure.timestamp),
            const SizedBox(
              width: CustomPadding.s,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    expenditure.name,
                    maxLines: 1,
                    style: Theme.of(context).textTheme.bodyLarge,
                  ),
                  Text(
                      expenditure.expeditureHomeCurrency.toString() +
                          " " +
                          (ExpendituresService().holiday?.homeCurrency ?? ""),
                      style: Theme.of(context).textTheme.labelMedium),
                  Text(
                      expenditure.expenditureLocalCurrency.toString() +
                          " " +
                          (ExpendituresService().holiday?.localCurrency ?? ""),
                      style: Theme.of(context).textTheme.labelMedium),
                  expenditure.tag.isEmpty
                      ? Container()
                      : Text(expenditure.tag,
                          style: Theme.of(context).textTheme.labelMedium),
                ],
              ),
            ),
            IconButton(
              onPressed: () => BottomSheetController().showBottomSheet(
                  AddExpenditureBottomSheet(expenditure: expenditure), context),
              icon: const Icon(Icons.edit),
            ),
            IconButton(
                onPressed: () =>
                    ExpendituresService().delete(expenditure.id, context),
                icon: const Icon(Icons.delete))
          ],
        ),
      ),
    );
  }
}
