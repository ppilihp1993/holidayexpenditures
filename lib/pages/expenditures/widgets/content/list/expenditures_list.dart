import 'package:flutter/material.dart';
import 'package:holiday_expentures/pages/expenditures/service/expenditures.service.dart';
import 'package:holiday_expentures/pages/expenditures/widgets/content/list/item/expenditures_list_item.dart';
import 'package:holiday_expentures/repository/expenditure/model/expenditure.model.dart';
import 'package:holiday_expentures/repository/holiday/holiday.repository.dart';
import 'package:holiday_expentures/utils/padding/padding.dart';

class ExpendituresList extends StatelessWidget {
  const ExpendituresList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Expenditure>>(
      initialData: const [],
      stream: HolidayRepository()
          .holidayExpenditures$(ExpendituresService().holiday?.id ?? ""),
      builder: (context, snapshot) {
        List<Expenditure> data = snapshot.data ?? [];
        if (data.isNotEmpty) {
          data.sort((a, b) => a.timestamp.compareTo(b.timestamp));
          data = data.reversed.toList();
        }

        return ListView.separated(
            itemBuilder: (context, index) =>
                ExpendituresListItem(expenditure: data[index]),
            separatorBuilder: (context, index) => const Padding(
                  padding: EdgeInsets.symmetric(vertical: CustomPadding.xs),
                  child: Divider(
                    color: Colors.black26,
                  ),
                ),
            itemCount: data.length);
      },
    );
  }
}
