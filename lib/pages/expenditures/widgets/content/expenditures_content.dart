import 'package:flutter/material.dart';
import 'package:holiday_expentures/pages/expenditures/widgets/content/list/expenditures_list.dart';
import 'package:holiday_expentures/pages/expenditures/widgets/content/summary/expenditures_summary.dart';
import 'package:holiday_expentures/utils/padding/padding.dart';

class ExpendituresContent extends StatelessWidget {
  ///
  ///
  const ExpendituresContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(CustomPadding.l),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: const [
            ExpendituresSummary(),
            SizedBox(
              height: CustomPadding.xl,
            ),
            Expanded(
              child: ExpendituresList(),
            )
          ],
        ),
      ),
    );
  }
}
