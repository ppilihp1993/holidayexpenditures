import 'package:flutter/material.dart';
import 'package:holiday_expentures/pages/expenditures/service/expenditures.service.dart';
import 'package:holiday_expentures/pages/expenditures/widgets/content/summary/expenditures_summary_element.dart';
import 'package:holiday_expentures/repository/expenditure/model/expenditure.model.dart';
import 'package:holiday_expentures/repository/holiday/holiday.repository.dart';

class ExpendituresSummary extends StatelessWidget {
  /// Subscription to the [holidayExpenditures$] stream
  ///
  const ExpendituresSummary({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Expenditure>>(
      initialData: const [],
      stream: HolidayRepository()
          .holidayExpenditures$(ExpendituresService().holiday?.id ?? ""),
      builder: (context, snapshot) {
        final data = snapshot.data ?? [];

        return ExpendituresSummaryElement(expenditures: data);
      },
    );
  }
}
