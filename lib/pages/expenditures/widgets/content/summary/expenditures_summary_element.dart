import 'package:flutter/material.dart';
import 'package:holiday_expentures/pages/expenditures/service/expenditures.service.dart';
import 'package:holiday_expentures/repository/expenditure/model/expenditure.model.dart';
import 'package:collection/collection.dart';

class ExpendituresSummaryElement extends StatefulWidget {
  /// List of all [Expenditure]'s
  ///
  final List<Expenditure> expenditures;

  /// Displays the expenditures summary
  ///
  /// THe summary currency can be changed via tap from home to local
  ///
  const ExpendituresSummaryElement({Key? key, required this.expenditures})
      : super(key: key);

  @override
  State<ExpendituresSummaryElement> createState() =>
      _ExpendituresSummaryElementState();
}

class _ExpendituresSummaryElementState
    extends State<ExpendituresSummaryElement> {
  ///
  ///
  bool _isHomeCurrency = false;

  /// Calculate the sum of the required elements
  ///
  String _calculateSummary() {
    final summary = widget.expenditures
        .map((e) => _isHomeCurrency
            ? e.expeditureHomeCurrency
            : e.expenditureLocalCurrency)
        .sum;
    return summary.toStringAsFixed(2) + " " + _getCurrencyShortcut();
  }

  /// Returns the currency shortcut
  ///
  String _getCurrencyShortcut() {
    return _isHomeCurrency
        ? (ExpendituresService().holiday?.homeCurrency ?? "")
        : (ExpendituresService().holiday?.localCurrency ?? "");
  }

  /// Change the currency boolean [_isHomeCurrency]
  ///
  void _changeCurrency() {
    _isHomeCurrency = !_isHomeCurrency;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final summary = _calculateSummary();

    return GestureDetector(
      onTap: () => _changeCurrency(),
      child: Text(
        summary,
        style: Theme.of(context).textTheme.headlineLarge,
      ),
    );
  }
}
