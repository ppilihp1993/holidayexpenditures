import 'package:flutter/material.dart';
import 'package:holiday_expentures/pages/expenditures/expenditures.page.dart';
import 'package:holiday_expentures/repository/holiday/model/holiday.model.dart';
import 'package:holiday_expentures/utils/padding/padding.dart';

class HolidayListItem extends StatelessWidget {
  /// Selected [Holiday] item
  ///
  final Holiday holiday;

  /// Item of [HolidayList] widget
  ///
  /// Contains the name of the [Holiday] element and a delete button
  ///
  const HolidayListItem({Key? key, required this.holiday}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: CustomPadding.s),
      child: GestureDetector(
        onTap: () => Navigator.of(context)
            .pushNamed(ExpenturesPage.route, arguments: holiday),
        child: Card(
          color: const Color.fromARGB(255, 229, 221, 248),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(CustomPadding.m)),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(CustomPadding.m,
                CustomPadding.xs, CustomPadding.xs, CustomPadding.xs),
            child: Row(
              children: [
                Expanded(
                    child: Text(
                  holiday.name,
                  style: Theme.of(context).textTheme.titleLarge,
                )),
                IconButton(
                    onPressed: () => "",
                        // HolidayRepository().deleteHoliday(holiday.id),
                    icon: const Icon(Icons.delete))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
