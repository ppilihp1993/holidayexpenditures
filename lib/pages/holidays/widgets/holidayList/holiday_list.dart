import 'package:flutter/material.dart';
import 'package:holiday_expentures/pages/holidays/widgets/holidayListItem/holiday_list_item.dart';
import 'package:holiday_expentures/repository/holiday/holiday.repository.dart';
import 'package:holiday_expentures/repository/holiday/model/holiday.model.dart';

class HolidayList extends StatelessWidget {
  /// [ListView] from the holidays [Stream]
  ///
  const HolidayList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Holiday>>(
      initialData: const [],
      stream: HolidayRepository().holidays$(),
      builder: (context, snapshot) {
        final data = snapshot.data ?? [];
        if (data.isEmpty) {
          return Container();
        } else {
          return ListView.builder(
            itemCount: data.length,
            itemBuilder: (context, index) =>
                HolidayListItem(holiday: data[index]),
          );
        }
      },
    );
  }
}
