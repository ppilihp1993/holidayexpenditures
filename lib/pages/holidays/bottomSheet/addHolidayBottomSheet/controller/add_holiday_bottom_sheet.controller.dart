import 'package:flutter/material.dart';
import 'package:holiday_expentures/controller/snackbar/custom_snackbar.dart';
import 'package:holiday_expentures/repository/holiday/holiday.repository.dart';
import 'package:holiday_expentures/repository/holiday/model/holiday.model.dart';
import 'package:holiday_expentures/repository/openExchange/open_exchange.repository.dart';
import 'package:holiday_expentures/utils/constants/constants.dart';

class AddHolidayBottomSheetController {
  /// [TextEditingController] for holiday name
  ///
  final TextEditingController textEditingController = TextEditingController();

  /// Currency shortcut
  ///
  String currencyShortcut = "";

  ///
  ///
  void create(BuildContext context) async {
    if (textEditingController.text.isEmpty && currencyShortcut.isEmpty) {
      CustomSnackBar.of(context).showError("Please set a title and currency");
    }

    final title = textEditingController.text;
    final exchangeRate = await _getExchangeRate();

    final holiday = Holiday.firebase(
        title, exchangeRate, currencyShortcut, exchangeShortcutEUR);
    final result = await HolidayRepository().createHoliday(holiday);

    result.fold(
        (l) => CustomSnackBar.of(context)
            .showError("Could not create new holiday element"),
        (r) => CustomSnackBarData(context)
            .showSuccess("Successfully created holiday"));

    Navigator.of(context).pop();
  }

  /// Calculate the exchange rate
  ///
  Future<double> _getExchangeRate() async {
    final exchangeRatesResult =
        await OpenExchangeRepository().fetchLatestExchangeRates();
    final exchangeRates = exchangeRatesResult.getOrElse(() => {});

    if (exchangeRates.isEmpty) {
      return Future.value(0.0);
    }

    final exchangeRateEur = exchangeRates[exchangeShortcutEUR] ?? 0;
    final exchangeRateNew = exchangeRates[currencyShortcut] ?? 0;

    if (exchangeRateEur == 0 || exchangeRateNew == 0) {
      return Future.value(0.0);
    }

    final double exchangeRate = ((1 + (1 - exchangeRateEur)) * exchangeRateNew);
    return exchangeRate;
  }
}
