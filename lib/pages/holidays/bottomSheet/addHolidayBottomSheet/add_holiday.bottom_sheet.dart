import 'package:flutter/material.dart';
import 'package:holiday_expentures/pages/holidays/bottomSheet/addHolidayBottomSheet/controller/add_holiday_bottom_sheet.controller.dart';
import 'package:holiday_expentures/pages/holidays/bottomSheet/addHolidayBottomSheet/widgets/currenyNameAutocomplete/currency_name_autocomplete.dart';
import 'package:holiday_expentures/utils/padding/padding.dart';
import 'package:holiday_expentures/widgets/bottomSheetTemplate/custom_bottom_sheet_template.dart';
import 'package:holiday_expentures/widgets/buttons/customElevatedButton/custom_elevated_button.dart';

class AddHolidayBottomSheet extends StatelessWidget {
  /// Instance of [AddHolidayBottomSheetController]
  ///
  final AddHolidayBottomSheetController controller;

  /// Widget for creating a new [HolidayElement]
  ///
  AddHolidayBottomSheet({Key? key})
      : controller = AddHolidayBottomSheetController(),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomBottomSheetTemplate(
      child: Column(
        children: [
          Text(
            "Create holiday",
            style: Theme.of(context).textTheme.titleMedium,
          ),
          const SizedBox(
            height: CustomPadding.l,
          ),
          TextField(
            controller: controller.textEditingController,
            decoration:
                const InputDecoration(label: Text("Title of your holiday")),
          ),
          const SizedBox(
            height: CustomPadding.l,
          ),
          CurrencyNameAutocomplete(
            callback: (currencyShortCut) =>
                controller.currencyShortcut = currencyShortCut,
          ),
          const SizedBox(
            height: CustomPadding.l,
          ),
          CustomElevatedButton(
            text: "Save",
            onTap: () => controller.create(context),
          )
        ],
      ),
    );
  }
}
