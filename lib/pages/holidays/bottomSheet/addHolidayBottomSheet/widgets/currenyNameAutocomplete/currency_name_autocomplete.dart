import 'package:flutter/material.dart';
import 'package:holiday_expentures/repository/openExchange/open_exchange.repository.dart';

class CurrencyNameAutocomplete extends StatefulWidget {
  /// Callback with the currency short version
  ///
  final Function(String currencyShortCut)? callback;

  ///
  ///
  const CurrencyNameAutocomplete({Key? key, this.callback}) : super(key: key);

  @override
  State<CurrencyNameAutocomplete> createState() =>
      _CurrencyNameAutocompleteState();
}

class _CurrencyNameAutocompleteState extends State<CurrencyNameAutocomplete> {
  /// All exchange names
  ///
  Map<String, String> _currencyNames = {};

  @override
  void initState() {
    _setupCurrencyNames();
    super.initState();
  }

  void _setupCurrencyNames() async {
    final result = await OpenExchangeRepository().fetchAllCurrencyNames();
    _currencyNames = result.getOrElse(() => {});
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return _currencyNames.isNotEmpty
        ? Autocomplete<String>(
            optionsBuilder: (textEditingValue) {
              return _currencyNames.values.where((element) => element
                  .toLowerCase()
                  .startsWith(textEditingValue.text.toLowerCase()));
            },
            onSelected: (option) => widget.callback?.call(_currencyNames.entries
                .firstWhere((element) => element.value == option,
                    orElse: () => const MapEntry("", ""))
                .key),
          )
        : Container();
  }
}
