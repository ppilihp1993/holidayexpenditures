import 'package:flutter/material.dart';
import 'package:holiday_expentures/controller/bottomSheet/bottom_sheet.controller.dart';
import 'package:holiday_expentures/pages/holidays/bottomSheet/addHolidayBottomSheet/add_holiday.bottom_sheet.dart';
import 'package:holiday_expentures/pages/holidays/widgets/holidayList/holiday_list.dart';
import 'package:holiday_expentures/utils/padding/padding.dart';
import 'package:holiday_expentures/widgets/buttons/customFloatingActionButton/custom_floating_action_button.dart';

class HolidaysPage extends StatelessWidget {
  /// Route name
  ///
  static const route = "/holidays";

  ///
  ///
  const HolidaysPage({Key? key}) : super(key: key);

  /// Open [AddHolidayBottomSheet]
  ///
  void _displayBottomSheet(BuildContext context) {
    BottomSheetController().showBottomSheet(AddHolidayBottomSheet(), context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: CustomFloatingActionButton(
        callback: () => _displayBottomSheet(context),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(CustomPadding.l),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                children: [
                  Image.asset('assets/appIcon/app_icon.png', height: 40),
                  const SizedBox(width: CustomPadding.l,),
                  Text(
                    "Holidays",
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                ],
              ),
              const SizedBox(
                height: CustomPadding.l,
              ),
              const Expanded(
                child: HolidayList(),
              )
            ],
          ),
        ),
      ),
    );
  }
}
