import 'package:flutter/material.dart';

class CustomFloatingActionButton extends StatelessWidget {
  /// On tap callback
  ///
  final Function()? callback;

  /// Custom [FloatingActionButton] with background color lime and an add icon as child
  ///
  const CustomFloatingActionButton({Key? key, this.callback}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      heroTag: UniqueKey(),
      onPressed: () => callback?.call(),
      backgroundColor: Colors.lime[300],
      child: const Icon(
        Icons.add,
        color: Colors.black,
      ),
    );
  }
}
