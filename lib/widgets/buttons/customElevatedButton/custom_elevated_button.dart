/*
 * Filename: /Users/philippschoenhuth/Projects/PersonalProjects/wotau/wotauFrontend/wotaufrontend/lib/widgets/buttons/wotau_elevated_button.dart
 * Path: /Users/philippschoenhuth
 * Created Date: Wednesday, December 14th 2022, 10:32:53 am
 * Author: Philipp Schoenhuth
 * 
 * Copyright (c) 2022 wotau
 */

import 'package:flutter/material.dart';
import 'package:holiday_expentures/utils/padding/padding.dart';

class CustomElevatedButton extends StatelessWidget {
  /// Button text
  ///
  final String text;

  /// Button tap callback
  ///
  final Function()? onTap;

  /// Width factor of the [ElevatedButton] to ensure the same size for each button
  ///
  /// Default value is 0.7
  ///
  final double widthFactor;

  /// Predefined Custom [ElevatedButton]
  ///
  /// Ensures the same UI style for all [ElevatedButton]
  ///
  const CustomElevatedButton({
    Key? key,
    this.text = "",
    this.onTap,
    this.widthFactor = 0.7,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FractionallySizedBox(
      widthFactor: widthFactor,
      child: ElevatedButton(
        onPressed: onTap,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: CustomPadding.m),
          child: Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(
                color: onTap != null ? Colors.white : Colors.grey[700]),
          ),
        ),
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.resolveWith((states) {
          if (states.contains(MaterialState.disabled)) {
            return Colors.grey[300];
          } else {
            return Theme.of(context).primaryColor;
          }
        })),
      ),
    );
  }
}
