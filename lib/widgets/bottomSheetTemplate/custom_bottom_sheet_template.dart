import 'package:flutter/material.dart';
import 'package:holiday_expentures/utils/padding/padding.dart';

class CustomBottomSheetTemplate extends StatelessWidget {
  /// Child widget
  ///
  final Widget child;

  /// Template for [BottomSheet]'s
  ///
  const CustomBottomSheetTemplate({Key? key, this.child = const SizedBox()})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints:
          BoxConstraints(maxHeight: MediaQuery.of(context).size.height * 0.93),
      child: Column(
        mainAxisSize: MediaQuery.of(context).viewInsets.bottom > 0
            ? MainAxisSize.max
            : MainAxisSize.min,
        children: [
          Padding(padding: const EdgeInsets.all(CustomPadding.m), child: child)
        ],
      ),
    );
  }
}
