import 'package:flutter/material.dart';
import 'package:holiday_expentures/utils/padding/padding.dart';
import 'package:holiday_expentures/widgets/bottomSheetTemplate/custom_bottom_sheet_template.dart';
import 'package:holiday_expentures/widgets/buttons/customElevatedButton/custom_elevated_button.dart';

class DeleteBottomSheet extends StatelessWidget {
  /// Prompt a bottom sheet with the question if the user really wants to delete an entry
  /// 
  /// Returns true or false
  ///
  const DeleteBottomSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomBottomSheetTemplate(
      child: Column(
        children: [
          Text(
            "Do you really want to delete this entry?",
            style: Theme.of(context).textTheme.titleMedium,
          ),
          const SizedBox(
            height: CustomPadding.l,
          ),
          CustomElevatedButton(
            text: "Yes",
            onTap: () => Navigator.of(context).pop(true),
          ),
          const SizedBox(
            height: CustomPadding.s,
          ),
          CustomElevatedButton(
            text: "Yes",
            onTap: () => Navigator.of(context).pop(false),
          ),
        ],
      ),
    );
  }
}
