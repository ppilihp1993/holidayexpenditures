import 'package:flutter/material.dart';

abstract class CustomSnackBar {
  /// Displays different variants of the [SnackBarData]
  /// - [showDefault] -> [SnackBar] with color primary
  /// - [showError] -> [SnackBar] with color [Colors.red]
  /// - [showSuccess] -> [SnackBar] with color [Colors.green]
  ///
  /// The duration is by default three seconds
  ///
  static CustomSnackBarData of(BuildContext context) =>
      CustomSnackBarData(context);
}

class CustomSnackBarData {
  /// Active [BuildContext]
  ///
  final BuildContext context;

  /// Constructor for [CustomSnackBarData] with the [BuildContext] as parameter
  ///
  /// Displays a [SnackBar] with values
  ///
  const CustomSnackBarData(this.context);

  /// [SnackBar] configuration with background color primary and text color [Colors.white]
  ///
  void showDefault(String message) {
    final snackBar = SnackBar(
      content: Text(
        message,
        style: const TextStyle(color: Colors.white),
      ),
      duration: const Duration(seconds: 3),
      backgroundColor: Theme.of(context).primaryColor,
    );

    _show(snackBar);
  }

  /// [SnackBar] configuration with background color [Colors.red] and text color [Colors.white]
  ///
  void showError(String message) {
    final snackBar = SnackBar(
      content: Text(
        message,
        style: const TextStyle(color: Colors.white),
      ),
      duration: const Duration(seconds: 3),
      backgroundColor: Colors.red,
    );

    _show(snackBar);
  }

  /// [SnackBar] configuration with background color [Colors.green] and text color [Colors.white]
  ///
  void showSuccess(String message) {
    final snackBar = SnackBar(
      content: Text(
        message,
        style: const TextStyle(color: Colors.black),
      ),
      duration: const Duration(seconds: 3),
      backgroundColor: Colors.green,
    );

    _show(snackBar);
  }

  /// Displays the inout value [SnackBar] with the [ScaffoldMessengerState]
  ///
  /// Removes current availables [SnackBar]s if a new one is triggered
  ///
  void _show(SnackBar snackBar) {
    ScaffoldMessengerState scaffoldMessenger = ScaffoldMessenger.of(context);
    try {
      scaffoldMessenger.removeCurrentSnackBar(
          reason: SnackBarClosedReason.remove);
    } catch (e) {
      debugPrint("Error snackbar");
    }

    scaffoldMessenger.showSnackBar(snackBar);
  }
}
