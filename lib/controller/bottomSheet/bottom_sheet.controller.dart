import 'package:flutter/material.dart';
import 'package:holiday_expentures/utils/padding/padding.dart';

class BottomSheetController {
  /// Custom [BottomSheet] template
  ///
  Future<T> showBottomSheet<T>(Widget bottomSheet, BuildContext context) async {
    final data = await showModalBottomSheet(
        context: context,
        builder: (context) => bottomSheet,
        barrierColor: Colors.black45,
        isScrollControlled: true,
        enableDrag: false,
        backgroundColor: Colors.white,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(CustomPadding.xl),
                topRight: Radius.circular(CustomPadding.xl))));
    return data;
  }
}
